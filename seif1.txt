.open magshimim.db;
create table StudentsInClasses(studentID int ,classID int);
create table Teachers(teacherID int ,teacherName string);
create table Categories(catID int ,catName string);
create table Classes(classID int ,className string,teacherID int,catID int);
create table Students(studentID int,studentName string,studentAge string,studentGrade string);